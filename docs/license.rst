License
=======

AltWalker is licensed under the GNU General Public License v3.0.

License Text
------------

.. include:: ../LICENSE
